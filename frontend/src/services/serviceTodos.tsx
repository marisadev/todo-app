import api  from '../utils/constants';

export const serviceGetToDo = async () => {
    const response = await api.get('/todos')
    return response.data
}

export const serviceAddToDo = async (request:any) => {
    const response = await api.post('/todos', request)
    return response.data
}

export const serviceDeleteToDo = async (id:any) => {
    const response = await api.delete(`/todos/${id}`)
    return response
}

export const serviceUpdateToDo = async (data:any) => {
    const response = await api.put(`/todos/${data.id}`, data);
    return response.data
}