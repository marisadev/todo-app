import React, { useEffect, useRef }   from 'react';
import { useSelector, useDispatch } from "react-redux";
import './App.scss';

import { getTodoState } from "./reducers/todos/selectors";

import { calculateTodo, getDataTodo } from "./reducers/todos/actions";
import { serviceGetToDo }  from "./services/serviceTodos";

import FilterTodosComponents from './components/filterTodos/filterTodos';
import TodoListComponents from './components/todo/todoList';

function App() {
  const dispatch = useDispatch();
 {/* call Service Get ToDo */}
  const callServiceGetToDo = async () => {
    await serviceGetToDo().then((result) => {
      dispatch(getDataTodo(result))
      dispatch(calculateTodo())
    }).catch((error) => {
      console.log("callServiceGetToDo error")
    })
  }
  
  useEffect(() => {
    callServiceGetToDo();
  }, [])

  const todosList = useSelector(getTodoState);
  // console.log(todosList)
  return (
    <div className="App">
      <div className="App-body">
        {/* procress */}
        <div className="progress-component set-gap-body">
          <p className="title-text">Progress</p>
          <div className="progress">
             {/* Calculate in Redux */}
              <div className="progress-bar" style={{width: todosList.percentComplete + "%"}}></div>
          </div>
          <p className="sub-text">{todosList.totalCompleted} completed</p>
        </div>

        {/* Title and Dropdown UI */}
        <div className="title-check-component set-gap-body">
          <div className="set-display-row-space-between">
            <div className="title-text">Task</div>
            <div className="dropdown-fillter">
              <FilterTodosComponents />
            </div>
          </div>
        </div>

        {/*To Do List UI */}
        <div className="title-check-component set-gap-body">
          <TodoListComponents dataList={todosList} />
        </div>

      </div>
    </div>
  );
}

export default App;
