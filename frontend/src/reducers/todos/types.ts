//1 เก็บการเก็บพวก type ต่างๆ ที่ใช้ใน module todo
export interface Todo {
    id : string,
    title : string,
    completed:boolean,
    edit:boolean
}

export interface TodoState {
    filter:string;
    totalCompleted:number;
    percentComplete:number;
    todos: Todo[];
}

export enum TodoActionType {
    GET_TODO = "GET_TODO",
    FILTER_TODO = "FILTER_TODO",
    ADD_TODO = "ADD_TODO",
    CHECK_TODO = "CHECK_TODO",
    DELETE_TODO = "DELETE_TODO",
    CALCULATE_TODO = "CALCULATE_TODO",
    ACTION_EDIT = "ACTION_EDIT",
}
export type TodoAction = TodoActionType;

export interface TodoActionInterface {
    type: TodoAction;
    payload: any;
}