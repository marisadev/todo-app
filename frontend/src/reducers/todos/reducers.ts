//3 โดยในไฟล์นี้จะเป็น การจัดการ state ของ module todo
import { Todo, TodoState, TodoActionType, TodoActionInterface } from "./types";

const initialState: TodoState = {
	filter:"-",
	totalCompleted: 0,
	percentComplete:0,
	todos: [],
};


const calculatorData = (data : any)=>{
	var result = {
		countCompleted:0,
		percentCompleted:0
	}
	let summaryTodo = data.length;
	if(data != null){
		data.filter((value:any) =>{
			if(value.completed) result.countCompleted++
		})
	}

    if(result.countCompleted != 0 || summaryTodo != 0){
		result.percentCompleted = (100 * result.countCompleted) / summaryTodo;
	}

	return result;
}

const mapDataTodo = (data:any) => {
	data.forEach((element:any) => {
		initialState.todos.push({
			completed: element.completed,
			edit:false,
			id: element.id,
			title: element.title
		})
	});

	return initialState.todos;
}

export function todoReducer(state = initialState, action: TodoActionInterface): TodoState {
	switch (action.type) {
		case TodoActionType.GET_TODO:
			let mapData = mapDataTodo(action.payload);	
			return { ...state, todos: mapData};
			

		case TodoActionType.ACTION_EDIT:			
		return {
			...state,
			todos: state.todos.map((todo, index) =>
				todo.id === action.payload ? {...todo ,edit: !todo.edit } : todo					
			)
		};

		case TodoActionType.CALCULATE_TODO:
			let result = calculatorData(state.todos);
			return { ...state, totalCompleted: result.countCompleted , percentComplete:result.percentCompleted };	

		case TodoActionType.FILTER_TODO:
			return { ...state, filter:action.payload };

		case TodoActionType.ADD_TODO:
			return { ...state, todos: [...state.todos, action.payload] };

		case TodoActionType.DELETE_TODO:
			return { ...state, todos: state.todos.filter((todo, index) => todo.id !== action.payload) };

		case TodoActionType.CHECK_TODO:			
				return {
					...state,
					todos: state.todos.map((todo, index) =>
						todo.id === action.payload ? {...todo ,completed: !todo.completed } : todo					
					)
				};

		default:
			return state;
	}
}

