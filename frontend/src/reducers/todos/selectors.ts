//4. ไฟล์นี้จะเป็นการ ส่งค่า state ออกไปหรือนำค่า state มาแปลงเป็นอะไรสักอย่าง แล้วส่งออกไป
import { RootState } from "..";

export function getTodoState(state: RootState) {
	return state.todoReducer;
}

