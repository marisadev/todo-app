//2 เก็บ action ต่างๆ ที่ใช้ใน module todo ซึ่งจะมี add, delete, check
import { Todo, TodoActionType, TodoActionInterface } from "./types";

export function filterTodo(filter: string): TodoActionInterface {
	return {
		type: TodoActionType.FILTER_TODO,
		payload: filter,
	};
}


export function addTodo(todo: Todo): TodoActionInterface {
	return {
		type: TodoActionType.ADD_TODO,
		payload: todo,
	};
}

export function deleteTodo(id: any): TodoActionInterface {
	return {
		type: TodoActionType.DELETE_TODO,
		payload: id,
	};
}

export function checkTodo(id: any): TodoActionInterface {
	return {
		type: TodoActionType.CHECK_TODO,
		payload: id,
	};
}

export function getDataTodo(data: []): TodoActionInterface {
	return {
		type: TodoActionType.GET_TODO,
		payload: data,
	};
}


export function calculateTodo(): TodoActionInterface {
	return {
		type: TodoActionType.CALCULATE_TODO,
		payload: null,
	};
}
