
//ไฟล์นี้จะเป็นการจัดการรวม state แต่ละ module ไว้ใน store
import { createStore, combineReducers } from "redux";

import { todoReducer } from "./todos/reducers";

const rootReducer = combineReducers({
	todoReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default function configureStore() {
	const store = createStore(rootReducer);

	return store;
}