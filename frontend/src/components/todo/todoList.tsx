import React, { useEffect, useState, useRef }   from 'react';

import './todo.scss';

import {TodoComponents} from "./todo";
import { NewTodoComponents} from "./addTodo";

import { getId } from "../../utils/common"; 

function TodoListComponents(dataList:any) {
  const [todoState, setTodoState] = useState([]);
  const ref = useRef(null);

  useEffect(()=>{
    setTodoState(dataList.dataList.todos)
  })

  let [refs, setRefs] = useState([]);
  let divRef = useRef({});

  
  let filter = dataList.dataList.filter; 
  filter = filter == "0" ? false :  filter == "1" ? true : "-"
 
  var filterData = todoState.filter((val:any) => filter == "-" ? val : (val.completed == filter))
  {/* render todo list by filter data */}
  return (
    <div className="w-100">
       {
        filterData.length > 0 ?
          filterData.map((todo:any, key:number) => {
              {/* loop render dotos list */}
              return <TodoComponents key={todo.id} props = {{ todo: todo, index:key}} forwardRef={divRef}/>
          })
          :
         (<div style={{marginBottom:"5%"}}>Not Founnd</div>)
       }
       {/* input add todo */}
       <NewTodoComponents id={getId(dataList.dataList.todos)}  />
    </div>
  );
}

export default TodoListComponents;
