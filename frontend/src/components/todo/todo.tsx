import React, { useRef, useEffect, useState } from 'react';
import { useDispatch } from "react-redux";
import { checkTodo, calculateTodo , deleteTodo } from "../../reducers/todos/actions";

import { serviceUpdateToDo, serviceDeleteToDo } from "../../services/serviceTodos";

import { setStyleAndClassUiComponent } from "../../utils/common";

var keyMenuOpen = -1;
var stateClickSave = false;
var stateTitle:any;

export const TodoComponents = (props:any, divRef:any) => {
  const ref = useRef(divRef);
  var todo = props.props.todo;
  var key = props.props.index

  var request = {
    completed: todo.completed,
    title: todo.title,
    id: todo.id
  }
  
  const dispatch = useDispatch();
  const [editTodoState, setEditTodoState] = useState("");

  useEffect(()=>{
    setEditTodoState(todo.title)
    stateTitle = todo.title;
  },[])

  useEffect(()=>{
    document.addEventListener("click", handleClickOutsideList);
    return () => {
      document.removeEventListener("click", handleClickOutsideList);
    };
  })

  const handleClickOutsideList = (event:any) => {
    if(ref.current && ref.current !== event.target){
      let titleTodo = document.getElementById("title-todo-"+keyMenuOpen) as HTMLInputElement;
      let checkboxTodo = document.getElementById("checkbox-todo-"+keyMenuOpen) as HTMLInputElement;
      if(checkboxTodo){
        if(checkboxTodo.value == "true"){
          titleTodo.classList.add("text-decoration")
        }
        else{
          titleTodo.classList.remove("text-decoration")
        }
      }
      setStyleAndClassUiComponent(key, "close_menu");

      if(event.target.innerText == "Edit"){
        setStyleAndClassUiComponent(keyMenuOpen, "button_edit"); 
      }
      else{
        setStyleAndClassUiComponent(keyMenuOpen, "button_save"); 
        if(event.target.innerText == "Save" && stateClickSave == true){
          setEditTodoState(editTodoState);
        }
        else if(event.target.innerText != "Save" && stateClickSave == false){
          setEditTodoState(todo.title);
        }
      }
    }
    else{
      OpenMenuDropdown(keyMenuOpen);
    }
  };


  const OpenMenuDropdown = (key:number) => () => {
    setStyleAndClassUiComponent(keyMenuOpen, "button_save"); 
    setStyleAndClassUiComponent(key, "open_menu");
    keyMenuOpen = key;  
  }  
  
  const handeOnChangeInputTodo = (event: React.ChangeEvent<HTMLInputElement>) => {
    let getTargetData = event.target as HTMLInputElement;
    setEditTodoState(getTargetData.value);
  };
  
  const handleEditBtn = (key:number, id:any) => (event: React.MouseEvent<HTMLElement>) => {
    setStyleAndClassUiComponent(key, "button_edit");
  };

  const handleCheckBox  = (key:number, data:any) => (event: React.ChangeEvent<HTMLInputElement>) => {

    dispatch(checkTodo(data.id));
    dispatch(calculateTodo())

    data.completed = !data.completed
    request.completed = data.completed

    serviceUpdateToDo(request).then((result) => {
    }).catch((error) => {
      console.log("serviceUpdateToDo error",error)
    })
  };

  
  const handleSaveBtn = (key:number, data:any) => (event: React.MouseEvent<HTMLElement>) => {
    stateClickSave = true;
    setStyleAndClassUiComponent(key, "button_save");

    let titleTodo = document.getElementById("title-todo-"+key) as HTMLInputElement;
    if(data.completed){
      titleTodo.classList.add("text-decoration")
    }
    else{
      titleTodo.classList.remove("text-decoration")
    }

    request.title = titleTodo.value;
    serviceUpdateToDo(request).then((result) => {}).catch((error) => {
      console.log("serviceUpdateToDo error",error)
    })
  }


  const handleDelete =  (index:any, id:any) => (event: React.MouseEvent<HTMLElement>) => {
      setStyleAndClassUiComponent(index, "button_delete");
      dispatch(deleteTodo(id));
      dispatch(calculateTodo())
      
      serviceDeleteToDo(id).then((result) => {
      }).catch((error) => {
        console.log("serviceDeleteToDo error")
      })
  };

  return (
    <div className="bg-todo-components" id={"todo"+key} >
      {/* detail todos */}
      <div className="w-100 display-flex">
        <label className="container-checkbox">
            <input type="checkbox" 
              defaultChecked={todo.completed}
              onChange={handleCheckBox(key, todo)}
              id = {"checkbox-todo-"+key}
              value={todo.completed}
            /> 
          <span className="checkmark"></span>
        </label>

        <label className="container-input">
          <input 
            id = {"title-todo-"+key}
            type={"text"} 
            value={editTodoState} 
            className={todo.completed ? "input text-decoration" : "input"}
            readOnly={true}
            onChange={handeOnChangeInputTodo}
          />
        </label>
      </div>

      {/* button procress */}
      <div className="dropdown-container-menu">
        <label className="container-button" id={"save-btn-todo-"+key}>
          <div className="save-btn" onClick={handleSaveBtn(key, todo)}>Save</div>
        </label>

         <div id={"container-menu-"+key}  className="container-menu" ref={ref}  onClick={OpenMenuDropdown(key)}>
          <span className="dot"></span>
          <span className="dot"></span>
          <span className="dot" ></span>
        </div>

        <div id={"menuDropdown-"+key} className="menu dropdown-content">
            <div onClick={handleEditBtn(key, todo.id)}>Edit</div>
            <div className="color-red" onClick={handleDelete(key, todo.id)}>Delete</div>
        </div>
     </div>

    </div>
  );
}



