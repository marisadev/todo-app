import React, { useRef, useEffect } from 'react';
import { useDispatch } from "react-redux";
import { addTodo, calculateTodo } from "../../reducers/todos/actions";

import { serviceAddToDo } from "../../services/serviceTodos";


export function NewTodoComponents(props:any) {
  const refCreateTodo = useRef(null);
  const dispatch = useDispatch();

  let componentInput = document.getElementById("btnSave") as HTMLInputElement;
  let newTodoInput = document.getElementById("newTodoInput") as HTMLInputElement;

  let request = {
    id: props.id,
    title: "",
    completed: false
  }

  const showbtnSave = () =>{
    componentInput.classList.add("show")
  }

  const hidebtnSave = () =>{
    componentInput.classList.remove("show")
    newTodoInput.value = ""; 
  }
  
  useEffect(() => {
    document.addEventListener("click", handleClickOutsideAdd);
    return () => {
      document.removeEventListener("click", handleClickOutsideAdd);
    };
  });

  const handleClickOutsideAdd = (event:any) => {
    {/* auto hide btn */}
    if (refCreateTodo.current && refCreateTodo.current !== event.target) {
      hidebtnSave();
    }
    else{
      showbtnSave();
    }
  };

  const handleSaveNew = (event: React.MouseEvent<HTMLElement>) => {
      request.title = newTodoInput.value;

      if(newTodoInput.value != ""){
        dispatch(addTodo({...request, edit:false}));
        dispatch(calculateTodo())

        newTodoInput.value = "";
        {/* call service */}
        serviceAddToDo(request).then((result) => {
          

        }).catch((error) => {
          console.log("callServiceGetToDo error")
        })
      }

  };


  return (
    <div className="bg-todo-components">
      <div className="w-100 display-flex">

        <label className="container-input">
          <input id="newTodoInput" className="input" type={"text"} ref={refCreateTodo}  placeholder="Add your todo" autoComplete="off"/>
        </label>

        <label id="btnSave" className="container-button">
          <div className="save-btn" onClick={handleSaveNew}>Save</div>
        </label>
      </div>

    </div>
  );
}
