import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from "react-redux";
import { filterTodo } from "../../reducers/todos/actions";

import './filterTodos.scss';

const itemOption = [
  {label: "All", value: "-"},
  {label: "Done", value: "1"},
  {label: "Undone", value: "0"}
];


const FilterTodosComponents = () => {
  const dispatch = useDispatch();
  const refFilter = useRef(null);
  
  const [filterTodos, setFilterTodos] = useState(itemOption[0].label);

  {/* set value filter in Redux */}
  const handeClickOptionFilter = (event: React.MouseEvent<HTMLElement>) => {
    let getTargetData = event.target as HTMLInputElement;
    
		setFilterTodos(getTargetData.innerHTML)
    dispatch(filterTodo(getTargetData.id));

    ClosefilterDropdown();
	};


  const OpenfilterDropdown = () => {
    let filterDropdown = document.getElementById("filterDropdown") as HTMLInputElement;
    filterDropdown.classList.toggle("show")
  }
  
  const ClosefilterDropdown = () => {
    let filterDropdown = document.getElementById("filterDropdown") as HTMLInputElement;
    filterDropdown.classList.remove("show")
  }

  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  });

  const handleClickOutside = (event:any) => {
    {/* auto close */}
    if (refFilter.current && refFilter.current !== event.target) {
      ClosefilterDropdown();
    }
  };

  return (
    <div id="filterTodo" className="dropdown">
      <div className="input-icons">
        <input id="inputDropdown" ref={refFilter} onClick={OpenfilterDropdown} value={filterTodos} className="dropinput" readOnly/>
        <i className="arrow down"></i>
      </div>
      <div id="filterDropdown" className="dropdown-content">
        { itemOption.map(value => <div key={value.value} id={value.value} onClick={handeClickOptionFilter}>{value.label}</div>) }
       </div>
    </div>
  );
}


export default FilterTodosComponents;