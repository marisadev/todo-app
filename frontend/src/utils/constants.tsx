import axios from "axios";

let urlService = 'http://localhost:3001';

export default axios.create({
    baseURL: urlService
})