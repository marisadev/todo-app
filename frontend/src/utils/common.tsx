export const getId = (data:any) =>{
     {/* generate id for insert new todo */}
    let id = "0";
    if(data.length > 0){
        let idSplit = data[data.length-1].id.split("_");
        if(idSplit.length == 1){
            id = idSplit[0] + "_" + 1;
        }
        else{
        id = idSplit[0] + "_" + (parseInt(idSplit[1]) + 1) ;
        }
    }
    return id;
}


export const setStyleAndClassUiComponent =(key:number, working:string)=>{
    if(key != -1){
        let componentDropdown = document.getElementById("menuDropdown-"+key) as HTMLInputElement;
        let buttonSave = document.getElementById("save-btn-todo-"+key) as HTMLInputElement;
        let containerMenu = document.getElementById("container-menu-"+key) as HTMLInputElement;
        let titleTodo = document.getElementById("title-todo-"+key) as HTMLInputElement;

        if(working == "open_menu"){
            componentDropdown.classList.toggle("show")
        }
        else if(working == "close_menu"){
            componentDropdown.classList.remove("show")
        }

        else if(working == "button_save"){
            buttonSave.classList.remove("show")
            containerMenu.style.display = "block";

            titleTodo.setAttribute("readonly", "true");
        }

        else if(working == "button_edit"){
            componentDropdown.classList.remove("show")
            buttonSave.classList.add("show")

            containerMenu.style.display = "none";

            titleTodo.classList.remove("text-decoration")
            titleTodo.focus();

            titleTodo.removeAttribute("readonly");
        }

        else if(working == "button_delete"){
            componentDropdown.classList.remove("show")
        }
    }
  }