# Getting Started with Create React App
npx create-react-app nameApp --template typescript

# Install
- SASS : npm i sass
- Concurrently (Run front & back(JSON Server)) : npm install concurrently
- React Redux : 
- npm install react-redux
- npm install @reduxjs/toolkit react-redux
- axios :npm install axios

# Run จะรันทั้ง Back เเละ front พร้อมกัน
เพราะ set คำสั่ง Concurrently ที่ไฟล์ package.json

- cd frontend
- npm start
